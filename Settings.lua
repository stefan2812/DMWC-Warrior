local DMW = DMW
DMW.Rotations.WARRIOR = {}
local Warrior = DMW.Rotations.WARRIOR
local UI = DMW.UI

function Warrior.Settings()
    UI.HUD.Options = {
        [1] = {
            NoMount = {
                [1] = {Text = "Use Mount |cFF00FF00On", Tooltip = ""},
                [2] = {Text = "Use Mount |cFFFFFF00Off", Tooltip = ""}
            }
        }
    }
	UI.AddHeader("General")
	UI.AddToggle("AutoTarget", "Auto Targets mobs while in Combat", false)
	UI.AddToggle("Dont waste RAGE", nil, false)
	UI.AddRange("Dump RAGE above", "Will Dump Rage after ", 0, 100, 5, 70, true)
	UI.AddToggle("Dismount to engage in Combat",nil,false,true)

	
	UI.AddHeader("Rotation Type")
	UI.AddToggle("Use Arms Rotation",nil, false)
	UI.AddToggle("Use Fury Rotation",nil, false)
	UI.AddToggle("Use Tank Rotation",nil, false, true)

	UI.AddHeader("Tanking")
	UI.AddToggle("Taunt",nil, false)
	UI.AddToggle("MockingBlow",nil, false)

	UI.AddHeader("Interrupt")
	UI.AddToggle("Use Pummel",nil, false)
	UI.AddToggle("Use ShieldBash",nil, false)

	UI.AddHeader("Spells")
	UI.AddToggle("MortalStrike", "Use MortalStrike", false)
	UI.AddToggle("Bloodthirst","Use Bloodthirst", false)
	UI.AddToggle("Whirlwind", "Use WW", false)
	UI.AddDropdown("WW at/above", "WW when more then # Targets ", {"1","2","3"}, "2")
	UI.AddToggle("Execute", "Use Execute", false)
	UI.AddToggle("Overpower", "Use Overpower", false)
	UI.AddToggle("Revenge", "Use Revenge", false)

	UI.AddHeader("Cooldowns")
	UI.AddToggle("Bloodrage", "Use Bloodrage when available", false)
	UI.AddToggle("Berserker Rage", "Use Berserker Rage", false)
	UI.AddToggle("SweepingStrikes", "Use SweepingStrikes Talent, when two Targets available", false)
	UI.AddToggle("Recklessness","Use Recklessness",false)
	UI.AddToggle("Deathwish","Use Deathwish",false)


	UI.AddHeader("Mobility")
	UI.AddToggle("Use Charge", "Auto Charges a selected Target when not in Combat", false)
	UI.AddToggle("Use Intercept", "Auto Intercepts a selected Target when in Combat", false)
		
	UI.AddHeader("Debuffs")
	UI.AddToggle("Rend", "Applies Rend debuff to Targets", false)
	UI.AddToggle("Spread Rend", "Spread Rend to all targets within 5yd", false)
	UI.AddToggle("Demo Shout", "Use Demoralizing Shout", false)
	UI.AddToggle("ThunderClap", "Use Thunderclap", false)
	UI.AddDropdown("Demo Shout at/above", "Demoshout when more then # Targets ", {"1","2","3","4","5"}, "2")
	UI.AddDropdown("ThunderClap at/above", "ThunderClap when more then # Targets ", {"1","2","3","4","5"}, "3")
	UI.AddToggle("SunderArmor", "Applies SunderArmor debuff to Targets", false,true)
	UI.AddDropdown("Apply # Stacks of Sunder Armor", "Apply # Stacks of Sunder Armor", {"1","2","3","4","5"}, "3")
	UI.AddToggle("Hamstring < 30% Enemy HP", "Use Hamstring on mobs below 30% to make them easier to catch", false, true)
	
	
	UI.AddHeader("Stance")
	UI.AddToggle("Use Defense Stance")
	UI.AddToggle("Use Berserk Stance")
	UI.AddToggle("Return to Battle Stance after Combat","Returns into Battlestance after Combat",false, true)
	
	UI.AddHeader("Defensives")
	UI.AddToggle("Use ShieldBlock", nil, true)
	UI.AddToggle("Use ShieldWall", nil, true)
	UI.AddRange("Shieldblock HP", nil, 30, 100, 10, 50)
	UI.AddRange("ShieldWall HP", nil, 1, 100, 5, 50)
	UI.AddToggle("Use LastStand", nil, true)
	UI.AddBlank()
	UI.AddRange("LastStand HP", nil, 1, 100, 5, 50)
	UI.AddBlank()
	UI.AddToggle("Retaliation 1T",nil,false)
	UI.AddToggle("Retaliation >= 3T",nil,false)
	UI.AddRange("Retaliation HP 1T", nil, 1, 100, 5, 50)
	UI.AddRange("Retaliation HP 3T", nil, 1, 100, 5, 50)
	
	UI.AddHeader("Consumables")
	UI.AddToggle("Use HP Potion",nil,false,true)
	UI.AddDropdown("HP Potion to use", "Use HP Potion", {"Minor Healing Potion","Lesser Healing Potion","Healing Potion","Greater Healing Potion","Superior Healing Potion", "Major Healing Potion"}, "1",true)
	UI.AddRange("Use Potion at #% HP", nil, 10, 100, 5, 50, true)
	UI.AddToggle("Use Best HP Potion", "Check back for Potions and use best available one")
	UI.AddHeader("Not for general use")
	UI.AddToggle("Debug","Adds Debug prints to Chat", false)
	UI.AddToggle("DumpUpSunder",nil,false)
	UI.AddToggle("E",nil,false)
	UI.AddToggle("Switch target when enough aggro",nil,false,true)
	UI.AddToggle("Use Racial", "Use Racial Abilities", false)
	UI.AddToggle("Use Healthstone", "Use Racial Abilities", false)
	UI.AddRange("Use Healthstone at #% HP", nil, 10, 100, 5, 50, true)
	

	UI.AddHeader("GMR-Tweaks")
	UI.AddToggle("Use Mount while Herbing","Do you want to use a Mount while gathering Herbs?",false,true)
	UI.AddDropdown("Select a Mount", "Select witch mount to use", {"Pinto Bridle ","Brown Horse Bridle","Chestnut Mare Bridle"}, "1",true)

end
