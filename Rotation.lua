local DMW = DMW
local Warrior = DMW.Rotations.WARRIOR
local Rotation = DMW.Helpers.Rotation
local Setting = DMW.Helpers.Rotation.Setting
local Player, Buff, Debuff, Spell, Stance, Target, Talent, Item, GCD, CDs, HUD, rageDanceCheck, timer, timer2
local Startmoney = GetMoney()
local init = false
local race = UnitRace("player");

local function Locals()
	Player = DMW.Player
	name, realm = UnitName("player")
	Path = (tostring(GetWoWDirectory()).."\\Interface\\Addons\\DMWC-Warrior\\logs\\"..tostring(name).." - Rotation.txt")
	GPath = (tostring(GetWoWDirectory()).."\\Interface\\Addons\\DMWC-Warrior\\logs\\"..tostring(name).." - Gold.txt")
    Buff = Player.Buffs
	HP = (Player.Health / Player.HealthMax) * 100
    Debuff = Player.Debuffs
    Spell = Player.Spells
    Talent = Player.Talents
    Item = Player.Items
    Target = Player.Target or false
    HUD = DMW.Settings.profile.HUD
	CDs = Player:CDs()
    if Talent.TacticalMastery.Rank >= 4 then
        rageDanceCheck = true
    else
        rageDanceCheck = false
	end
	if timer == nil then 
		timer = DMW.Time 
	end
end
local function OnEvent(self, event, isInitialLogin, isReloadingUi)
	if isInitialLogin or isReloadingUi then
			print("Reload or Login")
	else
		--RunMacroText ("/script ResetInstances()")
		--print("Instances should have been reseted")
	end
end
--local f = CreateFrame("Frame")
--f:RegisterEvent("PLAYER_ENTERING_WORLD")
--f:SetScript("OnEvent", OnEvent)

local RendImmune = {
    ["Undead"] = true,
    ["Elemental"] = true,
    ["Totem"] = true,
    ["Mechanical"] = true
}
local SunderImmune = {
	["Totem"] = true,
    ["Mechanical"] = true
}
local stanceCheckBattle = {
	["Overpower"] = true,
	["Hamstring"] = true,
	["MockingBlow"] = true,
	["Rend"] = true,
	["SunderArmor"] = true,
	["Retaliation"] = true,
	["SweepStrikes"] = true,
	["ThunderClap"] = true,
	["Charge"] = true,
	["Execute"] = true,
	["ShieldBash"] = true
}
local stanceCheckDefense = {
	["Rend"] = true,
	["SunderArmor"] = true,
	["Disarm"] = true,
	["Revenge"] = true,
	["ShieldBlock"] = true,
	["ShieldBash"] = true,
	["ShieldWall"] = true,
	["Taunt"] = true
}
local stanceCheckBers = {
	["Execute"] = true,
	["BersRage"] = true,
	["SunderArmor"] = true,
	["Hamstring"] = true,
	["Intercept"] = true,
    ["Pummel"] = true,
	["Recklessness"] = true,
    ["Whirlwind"] = true
}
local wasteExceptions = {
	["Execute"] = true
}
local function Potionuse()
	PotionName = Setting("HP Potion to use")
	if PotionName == 1 then
		PotionID = 118
	end
	if PotionName == 2 then
		PotionID = 858
	end
	if PotionName == 3 then
		PotionID = 929
	end
	if PotionName == 4 then
		PotionID = 1710
	end
	if PotionName == 5 then
		PotionID = 3928
	end
	if PotionName == 6 then
		PotionID = 13446
	end
end
local function Mount()
	if Setting("Select a Mount") == 1 then
		selectedMount = "Pinto Bridle"
		MountAura = "Pinto Horse"
	end
	if Setting("Select a Mount") == 2 then
		selectedMount = "Brown Horse Bridle"
		MountAura = "Brown Horse"
	end
	if Setting("Select a Mount") == 3 then
		selectedMount = "Chestnut Mare Bridle"
		MountAura = "Chestnut Mare"
	end
end
local function regularCast(spell, Unit, pool)
	-- Check Debug Settings --
	if Setting("Debug") then
		if spell ~= prevs then
			Timestamp = date("%b %d %H:%M:%S")
			if Target then
				debugs = tostring(Timestamp).." - Spell = "..tostring(spell).." , RagePre = "..tostring(Player.Power).." , RagePost = "..tostring(Player.Power - Spell[spell]:Cost()).." Target: "..tostring(Target.Name).."\n"
			else
				debugs = tostring(Timestamp).." - Spell = "..tostring(spell).." , RagePre = "..tostring(Player.Power).." , RagePost = "..tostring(Player.Power - Spell[spell]:Cost()).."\n"
			end
			print (tostring(debugs))
			WriteFile(Path,debugs,true,true)
			prevs = spell
		end
	end
	if pool then 
		if spell == "Execute" then
			if (Spell[spell]:Cost() + 5) > Player.Power then
				return true
			end
		else
			if Spell[spell]:Cost() > Player.Power then
				return true
			end
		end
	end
	if Spell[spell]:Cast(Unit) then
        return true
    end
end
local function DumpBeforeDance(value, spell)

	---------------------------
	-- Check Debug Settings ---

	if Setting("Debug") then
		if (value ~= prev) then
			Timestamp = date("%b %d %H:%M:%S")
			dumps = tostring(Timestamp).." - Dumping "..tostring(value).." Rage - Current: "..tostring(Player.Power).." Rage - to cast spell: "..tostring(spell).."\n"
			print(tostring(dumps))
			WriteFile(Path,dumps,true,true)
			prev = value
		end
	end

	---------------------------
	-- Dump base on Settings --

    if value >= 30 then
		if Setting("MortalStrike") and Spell.MortalStrike:Known() and Spell.MortalStrike:CD() == 0 then 
			if regularCast("MortalStrike", Target) then
				return true
			end
		end
		if Setting("Bloodthirst") and Spell.Bloodthirst:Known() and Spell.Bloodthirst:CD() == 0 then 
			if regularCast("Bloodthirst", Target) then
				return true
			end
		end
    elseif value >= 20 then
            if #Player:GetEnemies(5) >= 2 then
				if regularCast("ThunderClap", Player) then
					return true
				end
				if regularCast("Cleave", Target) then
					return true
				end
			else
				if Setting("DumpUpSunder") and not SunderImmune[Target.CreatureType] and Debuff.SunderArmor:Stacks(Target) < 5 then
					if regularCast("SunderArmor",Target) then
						print("DumpUpSunder")
						return true
					end
				else
					if regularCast("HeroicStrike",Target) then
						return true
					end
				end
			end
    elseif value >= 10 then
		if Spell.Hamstring:Cast(Target) and not Debuff.Hamstring:Exist(Target) then 
			return true 
		end
    end
end
local function stanceDanceCast(spell, Unit, stance)

	---------------------------------
	-- Check Talent Mastery Talent --

	if rageDanceCheck then

		-----------------------
		-- Check WW Settings --

		if Setting("Whirlwind") and spell == "Rend" and Spell.Whirlwind:CD() == 0 then
			return true
		end

		--------------------------
		-- Check Debug Settings --

		if Setting("Debug") then
			if spell ~= prevs then
				Timestamp = date("%b %d %H:%M:%S")
				debugs = tostring(Timestamp).." - Spell = "..tostring(spell).." , RagePre = "..tostring(Player.Power).." , RagePost = "..tostring(Player.Power - Spell[spell]:Cost()).." , Stance = "..tostring(stance).." Target: "..tostring(Target.Name).."\n"
				print (tostring(debugs))
				WriteFile(Path,debugs,true,true)
				prevs = spell
			end
		end

		-------------------------------------
		-- Check required Stance and Dance --

        if stance == 1 then
            if Spell.StanceBattle:Cast() then end
        elseif stance == 2 then
            if Spell.StanceDefense:Cast() then end
        elseif stance == 3 then
            if Spell.StanceBers:Cast() then end
        end
    end
end
local function smartCast(spell, Unit, pool)
	-- Check Pooling requirement --
	if pool and Spell[spell]:Cost() > Player.Power then
		if spell == "SweepStrikes" and not select(2,GetShapeshiftFormInfo(1)) then
            Spell.StanceBattle:Cast()
        end
		return true
	end

	-- Check Anti Waste Setting --
	if Setting("Dont waste RAGE") and Player.Power >= 31 and not wasteExceptions[spell] then
		if DumpBeforeDance(Player.Power - 25, spell) then
			return true
		end
	end

	if select(2,GetShapeshiftFormInfo(3)) and spell == "ShieldBlock" and Spell.BersRage:CD() == 0 then
		if regularCast("BersRage",Player) then
			return true
		end
		return true
	end

	timer = DMW.Time
	
	-- Check required Stance to Dance to --
	if select(2,GetShapeshiftFormInfo(1)) then
		if not stanceCheckBattle[spell] then
			if stanceCheckDefense[spell] then
				stanceDanceCast(spell, Unit, 2)
			elseif stanceCheckBers[spell] then
				stanceDanceCast(spell, Unit, 3)
			else
				if Spell[spell]:Cast(Unit) then return true end
			end
		else
			if Spell[spell]:Cast(Unit) then return true end
		end
	elseif select(2,GetShapeshiftFormInfo(3)) then
		if not stanceCheckBers[spell] then
			if stanceCheckBattle[spell] then
				stanceDanceCast(spell, Unit, 1)
			elseif stanceCheckDefense[spell] then
				stanceDanceCast(spell, Unit, 2)
			else
				if Spell[spell]:Cast(Unit) then return true end
			end
		else
			if Spell[spell]:Cast(Unit) then return true end
		end
	elseif select(2,GetShapeshiftFormInfo(2)) then
		if not stanceCheckDefense[spell] then
			if stanceCheckBattle[spell] then
				stanceDanceCast(spell, Unit, 1)
			elseif stanceCheckBers[spell] then
				stanceDanceCast(spell, Unit, 3)
			else
				if Spell[spell]:Cast(Unit) then return true end
			end
		else
			if Spell[spell]:Cast(Unit) then return true end
		end
	end
end
local function DebugSettings()
	-- Debug Setting --
	if init == false or init == nil then
		NewSession = "------------ Initializing new Session ------------\n"
		WriteFile(Path,NewSession,true,true)
		init = true
	end
	if not Player.Combat and Setting("Debug") then
		if not prevs == nil then  
			prevs = nil
		end
		if not prev == nil then
			prev = nil
		end
	end
	-- Threat Debug --
	--if Target then
	--	print (
	--		"isTanking: "..tostring(select(1,Target:UnitDetailedThreatSituation(Player))) ..
	--		" threatStatus: "..tostring(select(2,Target:UnitDetailedThreatSituation(Player))) ..
	--		" threatPercent: "..tostring(select(3,Target:UnitDetailedThreatSituation(Player))) ..
	--		" rawThreatPercent: "..tostring(select(4,Target:UnitDetailedThreatSituation(Player))) ..
	--		" threatValue: "..tostring(select(5,Target:UnitDetailedThreatSituation(Player)))
	--	)
	--end
end
local function Pace()
	-- Ccontrol dance pace --
	if DMW.Time <= timer + 0.4 then 
		return true 
	end
end
local function Defense1()
	-- Retaliation -- 
	if Setting("Retaliation 1T") and Spell.Retaliation:Known() and Spell.Retaliation:CD() == 0 and HP <= Setting("Retaliation HP 1T")  then
		if Player.Combat and #Player:GetEnemies(8) <= 2 then
			if smartCast("Retaliation", Player) then
				return true
			end
		end
	end
	if Setting("Retaliation >= 3T") and Spell.Retaliation:Known() and Spell.Retaliation:CD() == 0 and HP <= Setting("Retaliation HP 3T")  then
		if Player.Combat and #Player:GetEnemies(8) >= 3 then
			if smartCast("Retaliation", Player) then
				return true
			end
		end
	end
	-- ShieldWall --	
	if Setting("Use ShieldWall") and HP <= Setting("ShieldWall HP") and Spell.ShieldWall:CD() == 0 and Spell.ShieldWall:Known() then
		if IsEquippedItemType("Shields") then
			if smartCast("ShieldWall",Player) then
				return true
			end
		end
	end
end
local function Defense2()
	-- Defence Stance --
	if Setting("Use Defense Stance") and #Player:GetEnemies(5) >= 1 and not select(2,GetShapeshiftFormInfo(2)) and Spell.StanceDefense:Known() and not Setting("Use Tank Rotation") then
		regularCast("StanceDefense", Player)
	end
	-- Shield Block --
    if Setting("Use ShieldBlock") and HP < Setting("Shieldblock HP") and #Player:GetEnemies(5) >= 1 and Spell.ShieldBlock:Known() and Target and (Target.SwingMH == nil or Target.SwingMH <= 0.5) then
        if IsEquippedItemType("Shields") then
            smartCast("ShieldBlock", Player)
        end
    end
	-- LastStand --
	if Setting("Use LastStand") and HP <= Setting("LastStand HP") and Spell.LastStand:CD() == 0 and Spell.LastStand:Known() then
		regularCast("LastStand",Player)
	end
end
local function ReturnToBattle()
	-- Return to Battle Stance --
	if not select(2,GetShapeshiftFormInfo(1)) and Setting("Return to Battle Stance after Combat") and not Player.Combat and Spell.StanceBattle:Known() then
		if regularCast("StanceBattle", Player) then
			return true
		end
	end
end
local function AutoTarget()
	-- Auto Targeting --
	if Player.Combat and not (Target and Target.ValidEnemy) and #Player:GetEnemies(5) >= 1 and  Setting("AutoTarget") then
		TargetUnit(DMW.Attackable[1].unit)
	end
end
local function Buffing()
	-- Battleshout --
	if Player.Combat then
		if not Buff.BattleShout:Exist(Player) and Spell.BattleShout:Known() then
			if regularCast("BattleShout", Player, true) then
				return true
			end
		end
		if Setting("Demo Shout") and not Debuff.DemoShout:Exist(Target) then
			if #Player:GetEnemies(5) >= Setting("Demo Shout at/above") then
				if regularCast("DemoShout",Target) then
					return true
				end
			end
		end
	end
end
local function Interrupt()
	-- Interrupt Target with Pummel --
	if Target and Setting("Use Pummel") and Target:Interrupt() and Spell.Pummel:Known() and Spell.Pummel:CD() == 0 then
		if smartCast("Pummel",Target) then
			return true
		end
	end
	-- Interrupt surroundings with Pummel --
	if Setting("Use Pummel") and Spell.Pummel:Known()and Spell.Pummel:CD() == 0 then
		for _, Unit in ipairs(Player:GetEnemies(15)) do
			if Unit:Interrupt() then
				if smartCast("Pummel",Unit) then
					return true
				end
			end
		end
	end
	-- Interrupt Target with ShieldBash --
	if Target and Setting("Use ShieldBash") and Target:Interrupt() and Spell.ShieldBash:Known() and Spell.ShieldBash:CD() == 0 then
		if IsEquippedItemType("Shields") then
			if smartCast("ShieldBash",Target) then
				return true
			end
		end
	end
	-- Interrupt surroundings with Pummel --
	if Setting("Use ShieldBash") and Spell.ShieldBash:Known() and Spell.ShieldBash:CD() == 0 then
		if IsEquippedItemType("Shields") then
			for _, Unit in ipairs(Player:GetEnemies(15)) do
				if Unit:Interrupt() then
					if smartCast("ShieldBash",Unit) then
						return true
					end
				end
			end
		end
	end
	-- Interurpt Target with ConcBlow --
	if Target and  Spell.ConcBlow:IsReady() and Target:Interrupt() and not Target:IsBoss() then
		if regularCast("ConcBlow",Target) then
			return true
		end
	end
	-- Interrupt surroundings with ConcBlow --
	if Spell.ConcBlow:IsReady() then
		for _, Unit in ipairs(Player:GetEnemies(15)) do
			if Unit:Interrupt() then
				if regularCast("ConcBlow",Unit) then
					return true
				end
			end
		end
	end
end
local function UsePotion()
	-- Use specific potion --
	if Setting("Use HP Potion") then
		if GetItemCount(PotionID) >= 1 and GetItemCooldown(PotionID) == 0 then
			if HP <= Setting("Use Potion at #% HP") and Player.Combat then
				name = GetItemInfo(PotionID)
				RunMacroText("/use " .. name)
				return true
			end
		end
	end
	-- Use best available potion --
	if Setting("Use Best HP Potion available") then
		if HP <= Setting("Use Potion at #% HP") and Player.Combat then
			if GetItemCount(13446) >= 1 and GetItemCooldown(13446) == 0 then
				name = GetItemInfo(13446)
				RunMacroText("/use " .. name)
				return true 
			elseif GetItemCount(3928) >= 1 and GetItemCooldown(3928) == 0 then
				name = GetItemInfo(3928)
				RunMacroText("/use " .. name)
				return true
			elseif GetItemCount(1710) >= 1 and GetItemCooldown(1710) == 0 then
				name = GetItemInfo(1710)
				RunMacroText("/use " .. name)
				return true
			elseif GetItemCount(929) >= 1 and GetItemCooldown(929) == 0 then
				name = GetItemInfo(929)
				RunMacroText("/use " .. name)
				return true
			elseif GetItemCount(858) >= 1 and GetItemCooldown(858) == 0 then
				name = GetItemInfo(858)
				RunMacroText("/use " .. name)
				return true
			elseif GetItemCount(118) >= 1 and GetItemCooldown(118) == 0 then
				name = GetItemInfo(118)
				RunMacroText("/use " .. name)
				return true
			end
		end
	end
	-- Use Healthstone
	if Setting("Use Healthstone") then
		if GetItemCount(9421) >= 1 and GetItemCooldown(9421) == 0 then
			if HP <= Setting("Use Healthstone at #% HP") then
				RunMacroText("/use Major Healthstone")
			end
		end
	end
end
local function Threat()
	if Target then
		for _, Unit in ipairs(Player:GetEnemies(5)) do
			if not Unit:UnitDetailedThreatSituation(Player) then
				-- Taunt --
				if Setting("Taunt") and Spell.Taunt:Known() and Spell.Taunt:CD() == 0 and not Unit:AuraByID(7922) and not Unit:AuraByID(20560) and not Unit:AuraByID(355) then
					smartCast("Taunt",Unit)
				end
				-- Mockingblow --
				if Setting("MockingBlow") and Spell.MockingBlow:Known() and Spell.MockingBlow:CD() == 0 and not Unit:AuraByID(7922) and not Unit:AuraByID(355) and not Unit:AuraByID(20560) then
					smartCast("MockingBlow",Unit)
				end
			end
		end
	end
end
local function E()
	if Setting("E") and Target and Player.Combat and not Target.Friend then
		-- Bers Rage --
		if Setting("Berserker Rage") and Spell.BersRage:CD() == 0 then
			smartCast("BersRage",Player,true)
		end
		-- Bloodrage --
		if Setting("Bloodrage") and Spell.Bloodrage:IsReady() and HP >= 60 and Buff.BersRage:Exist(Player) then
			if regularCast("Bloodrage", Player) then
				return true
			end
		end
		if Setting("Bloodrage") and Spell.Bloodrage:IsReady() and HP >= 60 and not Setting("Berserker Rage") then
			if regularCast("Bloodrage", Player) then
				return true
			end
		end
	end
end
local function Racial()
	if Setting("Use Racial") then
		if race == "Tauren" and Player.Combat and #Player:GetEnemies(8) >= 2 and Spell.WarStomp:IsReady() then
			if Spell.WarStomp:Cast(Player) then
				return true
			end
		elseif race == "Orc" and Spell.BloodFury:IsReady() and Player.Combat and Target and Target.TTD > 2 then
			if Spell.BloodFury:Cast(Player) then
				return true
			end
		elseif race == "Undead" and Player:HasFlag(DMW.Enums.UnitFlags.Feared) and Spell.WillofTheForsaken:IsReady() then
			if Spell.BloodFury:Cast(Player) then
				return true
			end
		elseif race == "Troll" and Spell.BerserkingTroll:IsReady() and Player.Combat and Target then
			if Spell.BerserkingTroll:Cast(Player) then
				return true
			end
		end
	end
end
local function CombatTank()
	-- Execute -- 
	if Target and Target.HP <= 20 and Setting("Execute") then
		if smartCast("Execute", Target, true) then
			return true
		end
	end
	-- Auto Target --
	if Setting("AutoTarget") and Player.Combat and not Target then
		-- Auto Targeting --
		if Player.Combat and not (Target and Target.ValidEnemy) and #Player:GetEnemies(15) >= 1 and  Setting("AutoTarget") then
			TargetUnit(DMW.Attackable[1].unit)
		end
	end
	-- Switch Target on targeted aggro --
	if Setting(	"Switch target when enough aggro") and Target and Player:IsTanking(Target) and not select(3,Target:UnitDetailedThreatSituation(Player)) == nil and select(3,Target:UnitDetailedThreatSituation(Player)) <= 10 then
		for _, Unit in ipairs(Player:GetEnemies(5)) do
			if not Player:IsTanking(Unit) then
				TargetUnit(Unit.Pointer)
			end
		end
	end
	-- Charge --
	if not Player.Combat and Target and not Target.Friend then
		if Setting("Use Charge") and Target.Distance >= 8 and Target.Distance <= 25 and Target.Facing then
			smartCast("Charge", Target) 
		end
	end
	-- Auto Attack --
	if Target and Target.ValidEnemy and not IsCurrentSpell(6603) and Target.Distance <= 5 and Target:IsEnemy() then
		StartAttack(Target.Pointer)
	end
	-- Intercept --
	if Setting("Use Intercept") and Player.Combat and Target and Target:IsEnemy() and Target.Distance <= 25 and Target.Distance >= 5 and Spell.Charge:CD() <= 8 then
		if smartCast("Intercept",Target) then
			return true
		end
	end

	if Player.Combat and Target and Target:IsEnemy() then
		-- Bers Rage --
		if Setting("Berserker Rage") and Spell.BersRage:CD() == 0 then
			smartCast("BersRage",Player,true)
		end
		-- Bloodrage --
		if Setting("Bloodrage") and Spell.Bloodrage:IsReady() and HP >= 60 and Buff.BersRage:Exist(Player) then
			if regularCast("Bloodrage", Player) then
				return true
			end
		end
		if Setting("Bloodrage") and Spell.Bloodrage:IsReady() and HP >= 60 and not Setting("Berserker Rage") then
			if regularCast("Bloodrage", Player) then
				return true
			end
		end
		-- Def Stance --
		if not select(2,GetShapeshiftFormInfo(2)) and Spell.BersRage:CD() > 0 then
			regularCast("StanceDefense", Player)
		end
		if Spell.BersRage:CD() > 0 or not Setting("Berserker Rage") then
			-- Def Stance --
			if not select(2,GetShapeshiftFormInfo(2)) and Spell.BersRage:CD() > 0 then
				regularCast("StanceDefense", Player)
			end
			if Buffing() then
				return true
			end
		end
		if Interrupt() then
			return true
		end
		Threat()
		-- ThunderClap --
		if Setting("ThunderClap") and #Player:GetEnemies(8) >= Setting("ThunderClap at/above") then
			if Spell.ThunderClap:IsReady() and not Debuff.ThunderClap:Exist(Target) then
				if smartCast("ThunderClap",Player) then
					return true
				end
			end
		end
		-- ShieldSlam --
		if Spell.ShieldSlam:Known() and Spell.ShieldSlam:CD() == 0 then
			if smartCast("ShieldSlam",Target,true) then
				return true
			end
		end
		-- Bloodthirst --
		if Setting("Bloodthirst") and Spell.Bloodthirst:Known() and Spell.Bloodthirst:CD() == 0 then 
			if regularCast("Bloodthirst", Target) then
				return true
			end
		end
		-- Dump --
		if Player.Power >= Setting("Dump RAGE above") then
			if #Player:GetEnemies(8) > 1 then 
				if regularCast("Cleave",Target) then
					return true 
				end
			elseif Spell.ShieldSlam:Known() and Spell.ShieldSlam:CD() == 0 then
				if smartCast("ShieldSlam",Target,true) then
					return true
				end
			else
				if regularCast("HeroicStrike",Target) then
					return true 
				end
			end
		end
		-- SunderArmor --
		if Setting("SunderArmor") and Spell.SunderArmor:IsReady() and Debuff.SunderArmor:Stacks(Target) < Setting("Apply # Stacks of Sunder Armor") then
			if regularCast("SunderArmor",Target) then
				return true
			end
		end
		-- Rend --
		if Setting("Rend") and Spell.Rend:Known() and not RendImmune[Target.CreatureType] then
			if Target.TTD >= 2 and not Debuff.Rend:Exist(Target) then
				if smartCast("Rend",Target,true) then
					return true
				end
			end
			if Setting("Spread Rend") and #Player:GetEnemies(5) >= 2 then
				for _,Unit in ipairs(Player:GetEnemies(5)) do
					if not RendImmune[Unit.CreatureType] then
						if Unit.TTD >= 2 and not Debuff.Rend:Exist(Unit) then
							if smartCast("Rend",Unit,true) then
								return true
							end
						end
					end
				end
			end
		end
		-- Whirlwind --
		if Setting("Whirlwind") and Spell.Whirlwind:Known() and Spell.Whirlwind:CD() == 0 and #Player:GetEnemies(8) >= 2 then
			if smartCast("Whirlwind",Player,true) then
				return true
			end
		end
	end
end
local function CombatArms()
	-- Auto Target --
	if Setting("AutoTarget") and Player.Combat and not Target then
		AutoTarget()
	end
	-- Auto Charge --
	if not Player.Combat and Target and not Target.Friend  and not UnitIsDeadOrGhost("Target") then
		if Setting("Use Charge") and Target.Distance >= 8 and Target.Distance <= 25 and Target.Facing then
			smartCast("Charge", Target)
		end
	end
	if Player.Combat and Target and Target.ValidEnemy and not UnitIsDeadOrGhost("Target") and Target:IsEnemy() then
		-- Sweeping Strikes --
		if Setting("SweepingStrikes") then
			if Spell.SweepStrikes:Known() and Spell.SweepStrikes:CD()== 0 and #Player:GetEnemies(5) >= 2 then
				--if Setting("Debug") then
				--	PlaySound(416)
				--	print("Casting SweepStrikes because :"..tostring(#Player:GetEnemies(5)).." Enemies within 5yds")
				--end
				if smartCast("SweepStrikes",Player,true) then
					return true
				end
			end
		end
		if Buffing() then
			return true
		end
		-- Auto Attack --
		if not IsCurrentSpell(6603) and Target.Distance <= 5 then
			StartAttack(Target.Pointer)
		end
		-- Execute --
		if Target.HP < 20 then
			if Setting("Execute") and Spell.Execute:Known()	then
				if Setting("Recklessness") and Spell.Recklessness:Known() and Spell.Recklessness:CD() == 0 and Target:IsBoss() then
					if smartCast("Recklessness",Player) then
						return true
					end
				end
				if select(2,GetShapeshiftFormInfo(2)) then
					if smartCast("Execute", Target, true) then
						return true
					end
				else
					if regularCast("Execute", Target, true) then
						return true
					end
				end
			elseif not Setting("Execute") then
				if regularCast("HeroicStrike",Target,true)  then
					return true
				end
			end
		end
		-- Bloodrage --
		if Setting("Bloodrage") and Spell.Bloodrage:IsReady() then
			regularCast("Bloodrage", Player)
		end
		-- Bers Rage --
		if Setting("Berserker Rage") and Spell.BersRage:CD() == 0 and Target.TTD >= 4 and Spell.BersRage:Known() then
			smartCast("BersRage", Player)
		end
		-- Intercept --
		if Setting("Use Intercept") and Spell.Intercept:CD() == 0 and Target.Distance >= 8 and Target.Distance <= 25 and Player.Power >= 10 and Player.Power <= 25 and Spell.Charge:CD()<= 8 then
			if smartCast("Intercept",Target,true) then
				return true
			end
		end
		-- Hamstring --
		if Setting("Hamstring < 30% Enemy HP") and Player.Combat and Spell.Hamstring:Known() and Target.HP <= 30 and Target.Distance <= 5 and not Debuff.Hamstring:Exist(Target) and not (#Player.OverpowerUnit > 0 and Spell.Overpower:CD() == 0) then
			if smartCast("Hamstring", Target, true) then
				return true
			end
		end
		-- Whirlwind AoE --
		if Setting("Whirlwind") and Spell.Whirlwind:Known() and Spell.Whirlwind:CD() == 0 and #Player:GetEnemies(8) >= Setting("WW at/above") then
			if smartCast("Whirlwind",Player,true) then
				return true
			end
		end
		-- Mortalstrike --
		if Spell.MortalStrike:IsReady() then
			if regularCast("MortalStrike",Target, true) then
				return true
			end
		end
		-- SunderArmor --
		if Setting("SunderArmor") and Spell.SunderArmor:IsReady() and not SunderImmune[Target.CreatureType] then
			if Debuff.SunderArmor:Stacks(Target) < Setting("Apply # Stacks of Sunder Armor") and Target.TTD >= 2 then
				if regularCast("SunderArmor",Target,true) then
					return true
				end
			end
		end
		-- Rend --
		if Setting("Rend") and Spell.Rend:Known() and not RendImmune[Target.CreatureType] then
			if Target.TTD >= 2 and not Debuff.Rend:Exist(Target) then
				if smartCast("Rend",Target,true) then
					return true
				end
			end
			if Setting("Spread Rend") and #Player:GetEnemies(5) >= 2 then
				for _,Unit in ipairs(Player:GetEnemies(5)) do
					if not RendImmune[Unit.CreatureType] then
						if Unit.TTD >= 2 and not Debuff.Rend:Exist(Unit) then
							if smartCast("Rend",Unit,true) then
								return true
							end
						end
					end
				end
			end
		end
		-- Whirlwind -- 
		if Setting("Whirlwind") and Spell.Whirlwind:Known() and Spell.Whirlwind:CD() == 0 and Target.Distance <= 8 and #Player:GetEnemies(8) >= Setting("WW at/above") then
			if smartCast("Whirlwind",Player,true) then
				return true
			end
		end
		if (Spell.Whirlwind:CD() > .1 or not Spell.Whirlwind:Known() or not Setting("Whirlwind")) and (Spell.MortalStrike:CD() > .1 or not Spell.MortalStrike:Known() or not Setting("MortalStrike")) and (Spell.Bloodthirst:CD() > .1 or not Spell.Bloodthirst:Known() or not Setting("Bloodthirst")) and Player.Power >= Setting("Dump RAGE above") and Target.Distance <= 8 then
			if #Player:GetEnemies(5) >= 2 then
				if regularCast("Cleave",Target) then
					return true
				end
			else
				if Setting("DumpUpSunder") and not SunderImmune[Target.CreatureType] and Debuff.SunderArmor:Stacks(Target) < 5 then
					if regularCast("SunderArmor",Target) then
						print("DumpUpSunder")
						return true
					end
				else
					if regularCast("HeroicStrike",Target) then
						return true
					end
				end
			end
		end
	end
end
local function CombatFury()
	-- Auto Target --
	if Setting("AutoTarget") and Player.Combat and not Target then
		AutoTarget()
	end
	-- Auto Charge --
	if not Player.Combat and Target and not Target.Friend and not UnitIsDeadOrGhost("Target")then
		if Setting("Use Charge") and Target.Distance >= 8 and Target.Distance <= 25 and Target.Facing then
			smartCast("Charge", Target)
		end
	end
	if Player.Combat and Target and Target.ValidEnemy and not UnitIsDeadOrGhost("Target") and Target:IsEnemy() then
		-- Sweeping Strikes --
		if Setting("SweepingStrikes") then
			if Spell.SweepStrikes:Known() and Spell.SweepStrikes:CD()== 0 and #Player:GetEnemies(5) >= 2 then
				--if Setting("Debug") then
				--	PlaySound(416)
				--	print("Casting SweepStrikes because :"..tostring(#Player:GetEnemies(5)).." Enemies within 5yds")
				--end
				if smartCast("SweepStrikes",Player,true) then
					return true
				end
			end
		end
		if Buffing() then
			return true
		end
		-- Auto Attack --
		if not IsCurrentSpell(6603) and Target.Distance <= 5 then
			StartAttack(Target.Pointer)
		end
		-- Execute --
		if Setting("Execute") and Target:IsBoss() and Target.HP <= 21 and Spell.DeathWish:IsReady() then
			if regularCast("DeathWish") then
				return true
			end
		end
		if Target.HP < 20 then
			if Setting("Execute") and Spell.Execute:Known()	then
				if Setting("Recklessness") and Spell.Recklessness:Known() and Spell.Recklessness:CD() == 0 and Target:IsBoss() then
					if smartCast("Recklessness",Player) then
						return true
					end
				end
				if select(2,GetShapeshiftFormInfo(2)) then
					if smartCast("Execute", Target, true) then
						return true
					end
				else
					if regularCast("Execute", Target, true) then
						return true
					end
				end
			elseif not Setting("Execute") then
				if regularCast("HeroicStrike",Target,true)  then
					return true
				end
			end
		end
		-- Bloodrage --
		if Setting("Bloodrage") and Spell.Bloodrage:IsReady() then
			regularCast("Bloodrage", Player)
		end
		-- Bers Rage --
		if Setting("Berserker Rage") and Spell.BersRage:CD() == 0 and Target.TTD >= 4 and Spell.BersRage:Known() then
			smartCast("BersRage", Player)
		end
		-- Death Wish --
		if Setting("Deathwish") and HP > 40 and Target and Target.TTD > 4 then
			if regularCast("DeathWish") then
				return true
			end
		end
		-- Intercept --
		if Setting("Use Intercept") and Spell.Intercept:CD() == 0 and Target.Distance >= 8 and Target.Distance <= 25 and Player.Power >= 10 and Player.Power <= 25 then
			if smartCast("Intercept",Target,true) then
				return true
			end
		end
		-- Hamstring --
		if Setting("Hamstring < 30% Enemy HP") and Player.Combat and Spell.Hamstring:Known() and Target.HP <= 30 and Target.Distance <= 5 and not Debuff.Hamstring:Exist(Target) and not (#Player.OverpowerUnit > 0 and Spell.Overpower:CD() == 0) then
			if smartCast("Hamstring", Target, true) then
				return true
			end
		end
		-- Bloodthirst --
		if Spell.Bloodthirst:IsReady() then 
			if regularCast("Bloodthirst", Target) then
				return true
			end
		end
		-- Whirlwind --
		if Setting("Whirlwind") and Spell.Whirlwind:Known() and Spell.Whirlwind:CD() == 0 and Target.Distance <= 8 and #Player:GetEnemies(8) >= Setting("WW at/above") then
			if smartCast("Whirlwind",Player,true) then
				return true
			end
		end
		-- SunderArmor --
		if Setting("SunderArmor") and Spell.SunderArmor:IsReady() and not SunderImmune[Target.CreatureType] then
			if Debuff.SunderArmor:Stacks(Target) < Setting("Apply # Stacks of Sunder Armor") and Target.TTD >= 4 then
				if regularCast("SunderArmor",Target,true) then
					return true
				end
			end
		end
		-- Rend --
		if Setting("Rend") and Spell.Rend:Known() and not RendImmune[Target.CreatureType] then
			if Target.TTD >= 2 and not Debuff.Rend:Exist(Target) then
				if smartCast("Rend",Target,true) then
					return true
				end
			end
			if Setting("Spread Rend") and #Player:GetEnemies(5) >= 2 then
				for _,Unit in ipairs(Player:GetEnemies(5)) do
					if not RendImmune[Unit.CreatureType] then
						if Unit.TTD >= 2 and not Debuff.Rend:Exist(Unit) then
							if smartCast("Rend",Unit,true) then
								return true
							end
						end
					end
				end
			end
		end
		-- Whirlwind #2 -- 
		if (Spell.Whirlwind:CD() > .1 or not Spell.Whirlwind:Known() or not Setting("Whirlwind")) and (Spell.MortalStrike:CD() > .1 or not Spell.MortalStrike:Known() or not Setting("MortalStrike")) and (Spell.Bloodthirst:CD() > .1 or not Spell.Bloodthirst:Known() or not Setting("Bloodthirst")) and Player.Power >= Setting("Dump RAGE above") and Target.Distance <= 8 then
			if #Player:GetEnemies(5) >= 2 then
				if regularCast("Cleave",Target) then
					return true
				end
			else
				if Setting("DumpUpSunder") and not SunderImmune[Target.CreatureType] and Debuff.SunderArmor:Stacks(Target) < 5 then
					if regularCast("SunderArmor",Target) then
						print("DumpUpSunder")
						return true
					end
				else
					if regularCast("HeroicStrike",Target) then
						return true
					end
				end
			end
		end
	end
end
local function MountWhileHerbing()
	local checkHerb
	for _, Object in pairs(DMW.GameObjects) do
		if (Object.Herb) and Object.Distance <= 15 then
			if IsMounted() then
				RunMacroText("/cancelaura " .. MountAura)
			end
			herbObject = Object
		end
		if checkHerb ~= true then
			checkHerb = herbObject == Object and true or false 
		end
	end
	if not checkHerb and not IsMounted() and not Player.Combat and not Player.Moving and HP >= 60 then
		RunMacroText("/use " .. selectedMount)
	end
end
local function Counter()
	-- Revenge --
	if Setting("Revenge") then
		if Spell.Revenge:IsReady() then
			if smartCast("Revenge", Unit, true) then
				return true
			end
		end
	end
	-- OVERPOWER --
	if Setting("Overpower") then
		for _,Unit in ipairs(Player:GetEnemies(5)) do
			if Player.OverpowerUnit[Unit.Pointer] ~= nil then
				if smartCast("Overpower", Unit, true) then
					return true
				end
			end
		end
	end
end

function Warrior.Rotation()
	Locals()
	Potionuse()
	Mount()
	DebugSettings()
	--debugGold()

	if Setting("Use Mount while Herbing") and HUD.NoMount == 1 then
		MountWhileHerbing()
	end

	if IsMounted() and (Player.Combat or (Player.Target and Target.Distance <= 25 and Target.Distance >= 5) and Target:IsEnemy() and not UnitIsDeadOrGhost("Target"))  then
		RunMacroText("/cancelaura " .. MountAura)
	end

	if E() then
		return true
	end
	
	if UsePotion() then
		return true
	end
	
	if Pace() then
		return true
	end

	if Interrupt() then
		return true
	end
	
	if ReturnToBattle() then
		return true
	end

	AutoTarget()

	if not IsCurrentSpell(6603) and Player.Target and Target.Distance <= 5 and not UnitIsDeadOrGhost("Target") and Target:IsEnemy() then
		StartAttack(Target.Pointer)
	end

	if Defense1() then
		return true
	end

	Defense2()

	if Counter() then
		return true
	end
	if Racial() then
		return true
	end
	if Setting("Use Arms Rotation") and not Setting("Use Tank Rotation") and not Setting("Use Fury Rotation") then
		if CombatArms() then
			return true
		end
	elseif Setting("Use Tank Rotation") and not Setting("Use Arms Rotation") and not Setting("Use Fury Rotation") then
		Threat()
		if CombatTank() then
			return true
		end
	elseif Setting("Use Fury Rotation") and not Setting("Use Arms Rotation") and not Setting("Use Tank Rotation") then
		if CombatFury() then
			return true
		end
	elseif not Setting("Use DPS Rotation") and not Setting("Use Tank Rotation") and not Setting("Use Fury Rotation") then
			print ("Please go to DMWC Settings and select a rotation type. No combat rotation selected!")
	else
			print ("Please go to DMWC Settings and select only 1 rotation type. Currently more than 1 rotation selected!")
	end
end -- Rotation end
